#ifndef CLIENT_H

#define CLIENT_H

#include "unp_readline.c"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <time.h>


/* Server messages */
char *serv_rep_buffer;
char *SV_MSG;
char *SV_AMSG;
char *SV_CON_REP;
char *SV_CON_AMSG;
char *SV_DISC_AMSG;
char *SV_DISC_REP;

/* Client messages */
char *cl_msg_buffer;
char *CL_MSG;
char *CL_CON_REQ;
char *CL_PING_REP;
char *CL_DISC_REQ;


int client_socket;
int error;
struct sockaddr_in serv_sock_addr;

/* thread for permanent output */
pthread_t thread;

   
char *name;
char *buff;

in_port_t serv_port;
char *serv_addr; 

int request;
int err;
int len;
int flen;

int *printmsg();
#endif
