#include "Client.h"

void new_user_msg()
{ 
    
    uint16_t name_length;
    char *enter_user;
    /* Raumlänge und Raumname */
    memcpy(&name_length, SV_CON_AMSG + 1 , sizeof(uint16_t));
    name_length = ntohs(name_length);  

    enter_user = malloc(name_length);

    strncpy( enter_user, SV_CON_AMSG + 3, name_length);
    
    printf("<%s> hat den Chat betreten.\n", enter_user);
}


/* Username validation */
int isValidUsername(char *str)
{
    int i;
    for (i = 0; i<strlen(str); i++)
        if (!isalnum(str[i]))
        {
            fprintf(stderr, "Invalid Username. Only Alphanumeric values allowed\n");
            return 0;
        }
    printf("Valid username: %s\n", str);
    return 1;

}

/* Portnumber validation */
int isValidPort(char *str)
{
    int tmp_port;
    tmp_port = atoi(str);
    if(tmp_port < 1024 || tmp_port > 65535)
    {
        printf("Invalid port number. Port hast do be between 1024 and 65535.\n");
        return 0;
    }
    printf("Valid port: %i\n", tmp_port);
    return 1;
}

/* IP4address validation */ 
int isValidIP4Address(char *str)
{
    int valid;
    valid = inet_aton(str);

    if (valid == 0){
        printf("Invalid IP4 Address. IP Address looks like 0-255.0-255.0-255.0-255\n");
        return 0;
    }
    printf("Valid IP4 Addres: %s\n", str);
    return 1;

}

int main(int argc, char** argv)
{
    
    /* Init pointer */

    SV_CON_REP = NULL;
    CL_CON_REQ = NULL;

    /* read parameteroptions and values */

    int c;
    int index;

    opterr = 0;
    while ((c = getopt(argc, argv, "s:p:u:")) != -1)
        if ( argc == 7 )
        {
            switch(c)
                {
                case 's':
                    if (isValidIP4Address(optarg) != 0)
                    {
                        serv_addr = optarg;
                        break;
                    }
                    return 1;
                case 'p':
                    if (isValidPort(optarg) != 0){
                        serv_port = atoi(optarg);
                        break;
                    }
                    return 1;
                case 'u':
                    if (isValidUsername(optarg) != 0)
                    {
                        name = optarg;
                        break;
                    }
                    return 1;
                case ':':
                    fprintf (stderr, "Option -%c requires an argument value.\n", optopt);
                    return 1;
                case '?':
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                    return 1;
                }
            }
            else
            {
                printf ("Wrong number of arguments: \nUse udp_chat_client -p <port_address> -s <serv_addr> -u <username>\n");
                return 1;
            }

    client_socket = socket(AF_INET, SOCK_DGRAM, 0);
    if (client_socket < 1 )
    {
        perror("Create socket!\n");
    } else printf("Socket bound!\n");
    
    serv_sock_addr.sin_family = AF_INET;
    serv_sock_addr.sin_port = htons(serv_port);
    serv_sock_addr.sin_addr.s_addr = inet_addr(serv_addr);
    /* create CL_CON_REQ with id 1 (1), name length (2) and the name */
    uint8_t ID = 1;
    uint16_t name_length = htons(strlen(name));
    CL_CON_REQ = malloc(strlen(name) + sizeof(uint8_t) + sizeof(uint16_t));
    memcpy(CL_CON_REQ, &ID, sizeof(uint8_t)); 
    memcpy(CL_CON_REQ + 1, &name_length, sizeof(uint16_t));
    memcpy(CL_CON_REQ + 3, name, strlen(name));
    int len = 3 + strlen(name);

    sendto(client_socket, 0, 0, 0, &serv_sock_addr, sizeof(struct sockaddr_in));

    /* Thread for continious server msg output */
    pthread_create(&thread, NULL ,printmsg, NULL);



    /* loop in order to open a connection to the server */
    for (request = 0; request < 3; request++)
    {
        printf("Verbinde als %s zu Server (%s) auf port %u\n", name, serv_addr, serv_port);
        err = sendto(client_socket, CL_CON_REQ, len, 0, &serv_sock_addr, sizeof(struct sockaddr_in));   
        sleep(1);
        if (err > 0 && SV_CON_REP != NULL)
        {

            if(SV_CON_REP[1] == 0)
            {

                in_port_t new_port;
                memcpy(&new_port, SV_CON_REP + 2 , sizeof(uint16_t));
                
                /* save new port */  
                serv_sock_addr.sin_port = new_port;
                printf("Verbindung akzeptiert. Der Port für die weitere Kommunikation lautet %u\n", ntohs(new_port));
                              
                break;
                
            }
            else if(SV_CON_REP[1] == 1)
            {
                printf("Verbindung abgelehnt. \n");
                exit(0);

            }

        }
        if(request == 2 && SV_CON_REP == NULL)
        {
            int cl = close(client_socket);
            if(cl == -1)
            {
                printf("Socket konnte nicht beendet werden. Verbindung fehlgeschlagen. Wartezeit verstrichen\n");
            }
            printf("Verbindung fehlgeschlagen. Server antwortet nicht.\n");
            exit(0);
        }
        sleep(5);

    }
    
    /* Wait for user Input */
    while(1)
    {
        cl_msg_buffer = malloc(256);
        readline(0, cl_msg_buffer, 256);

        CL_MSG = malloc(strlen(cl_msg_buffer));

        if(strncmp(cl_msg_buffer, "/disconnect", 11) == 0)
        {

            uint8_t ID = 6;
            CL_DISC_REQ = malloc(1);
            for( request = 0; request < 2; request ++)
            {
                memcpy(CL_DISC_REQ, &ID, sizeof(uint8_t));
                printf("Beende die Verbindung zu Server (%s).\n", serv_addr);
                err = sendto(client_socket, CL_DISC_REQ, 1, 0, &serv_sock_addr, sizeof(struct sockaddr_in)); 
                sleep(5);
                if ( err < 0)
                {
                    printf("Fehler beim senden der CL_DSIC_REQ Nachricht.");
                }
                
            }
            printf("Verbindung nicht erfolgreich beendet. Timeout.\n");
            exit(0);      
        }
        else
        {
            /* create and sent message */

            uint8_t ID = 4;
            memcpy(CL_MSG, &ID, sizeof(uint8_t));

            uint32_t msg_length = strlen(cl_msg_buffer) - 1;
            msg_length = htonl(msg_length);
            memcpy(CL_MSG + 1, &msg_length, sizeof(uint32_t));
            /* memcpy without control char */
            memcpy(CL_MSG + 5, cl_msg_buffer, strlen(cl_msg_buffer) - 1);

            int len = 5 + strlen(cl_msg_buffer);
        
            err = sendto(client_socket, CL_MSG, len, 0, &serv_sock_addr, sizeof(struct sockaddr_in)); 

            if (err < 0)
            {
                printf("Konnte nicht gesendet werden.\n");
            }
        }
        free(cl_msg_buffer);
    }

    free(CL_CON_REQ);

}


int *printmsg()
{    
    while(1)
    {
        serv_rep_buffer = malloc(64);
        int serv_rep_len = recvfrom(client_socket, serv_rep_buffer, 64, 0, (struct sockaddr*) &serv_sock_addr, &flen);
        
        uint8_t ID;
        memcpy(&ID, serv_rep_buffer, sizeof(uint8_t));
        // printf("serv rep len: %i SV_REP_ID: %hu\n", serv_rep_len, ID);
        switch(ID)
        {
            /* server connection response */
            case 2:
            {
                SV_CON_REP = malloc(serv_rep_len);
                memcpy(SV_CON_REP , serv_rep_buffer, serv_rep_len);
                break;
            }

            /* SV_CON_AMSG */
            case 3:
            {
                SV_CON_AMSG = malloc(serv_rep_len);
                memcpy(SV_CON_AMSG, serv_rep_buffer, serv_rep_len);
                new_user_msg();
                break;
            }
            
            /* SV_AMSG */
            case 5:
            {
                SV_AMSG = malloc(serv_rep_len);
                memcpy(SV_AMSG, serv_rep_buffer, serv_rep_len);

                uint16_t name_length;
                memcpy(&name_length, SV_AMSG + 1, sizeof(uint16_t));
                name_length = ntohs(name_length);

                char *username = malloc(name_length);
                strncpy(username, SV_AMSG + 3, name_length);

                uint32_t msg_length;
                memcpy(&msg_length, SV_AMSG + 3 + name_length, sizeof(uint32_t));
                msg_length = ntohl(msg_length);

                char *usermsg = malloc(msg_length);
                strncpy(usermsg, SV_AMSG + 3 + name_length + 4, msg_length);

                printf("<%s>: %s\n", username, usermsg);
                break;

            }

            case 7:
            {
                printf("Verbindung erfolgreich beendet.\n");
                exit(0);
            }


            /* Disc msg */
            case 8:
            {
                SV_DISC_AMSG = malloc(serv_rep_len);
                memcpy(SV_DISC_AMSG, serv_rep_buffer, serv_rep_len);

                uint16_t name_length;
                memcpy(&name_length, SV_DISC_AMSG + 1, sizeof(uint16_t));
                name_length = ntohs(name_length);

                char *username = malloc(name_length);
                strncpy(username, SV_DISC_AMSG + 3, name_length);

                printf("<%s> hat den Server verlassen.\n", username);
                break;

            }

            /* Keep alive ping exchange */
            case 9:
            {
                uint8_t ID = 10;
                CL_PING_REP = malloc(sizeof(uint8_t));
                memcpy(CL_PING_REP, &ID, sizeof(uint8_t));
                err = sendto(client_socket, CL_PING_REP, sizeof(CL_PING_REP), 0, &serv_sock_addr, sizeof(struct sockaddr_in));
                if (err == -1)
                    printf("ERROR: Cannot send CL_PING_REP to server.\n");
                break;
            }
            
            /* Server msgs */
            case 11:
            {
                SV_MSG = malloc(serv_rep_len);
                memcpy(SV_MSG, serv_rep_buffer, serv_rep_len);
                
                uint32_t msg_length;
                memcpy(&msg_length, SV_MSG + 1 , sizeof(uint32_t));
                msg_length = ntohl(msg_length);
                
                char *msg = malloc(msg_length);
                strcpy(msg, SV_MSG + 5);

                printf("server: %s\n", msg);
                break;
            }
                        
        }
        sleep(1);
    }
}