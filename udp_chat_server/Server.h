#ifndef SERVER_H

#define SERVER_H

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* Server messages */
char *serv_msg_buffer;
char *SV_MSG;
char *SV_AMSG;
char *SV_CON_REP;
char *SV_CON_AMSG;
char *SV_DISC_AMSG;
char *SV_DISC_REP;
char *SV_PING_REQ;

/* Client messages */
char *cl_rep_buffer;
char *CL_MSG;
char *CL_CON_REQ;
char *CL_PING_REP;
char *CL_DISC_REQ;

int server_socket;
struct sockaddr_in from, to;
struct sockaddr_in server_socket_address;
in_port_t serv_port;
uint32_t usercount;
uint8_t ID;
fd_set readset;
struct timeval *time_to_wait;

int err, highsock;

struct user *user;
struct user *new_user;

struct user{
	char name[64];
	char IP[32];
	uint16_t port;
	struct sockaddr_in new_sv_sock_addr;
	int sv_socket;
	struct user *next;
	uint8_t rep_counter;
};

#endif