##################################################
#
# Makefile for program chat
#
##################################################


compile:
	cd udp_chat_server; make; 
	cd udp_chat_client; make;

clean: 
	cd udp_chat_server; make clean;
	cd udp_chat_client; make clean;